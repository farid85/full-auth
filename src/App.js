import {
  BrowserRouter as Router,
  Routes,
  Route
} from "react-router-dom";
import Activate from "./components/Activate";
import Home from "./components/Home";
import Login from "./components/Login";
import Admin from "./components/Admin";
import Private from "./components/Private";
import Register from "./components/Register";
import AdminRoute from "./routes/AdminRoute";
import PrivateRoute from "./routes/PrivateRoute";
import ForgetPassword from "./components/ForgetPassword";
import ResetPassword from "./components/ResetPassword";

function App() {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/register" element={<Register />} />
          <Route path='/users/activate/:token' element={<Activate />} />
          <Route path='/login' element={<Login />} />
          <Route path='/users/password/forget' element={<ForgetPassword />} />
          <Route path='/users/password/reset/:token' element={<ResetPassword />} />
          <Route
            path="/private"
            element={
              <PrivateRoute>
                <Private />
              </PrivateRoute>
            }
          />
          <Route
            path="/admin"
            element={
              <AdminRoute>
                <Admin />
              </AdminRoute>
            }
          />
        </Routes>
      </Router>,
    </div>
  );
}

export default App;
