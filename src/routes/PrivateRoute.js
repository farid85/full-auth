import React, { Component } from 'react';
import { Route, Navigate } from 'react-router-dom';
import { isAuth } from '../helpers/auth';

const PrivateRoute = ({ children }) => (
    isAuth ? children : <Navigate to="/login" />

);

export default PrivateRoute;