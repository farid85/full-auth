import React, { Component } from 'react';
import { Route, Navigate } from 'react-router-dom';
import { isAuth } from '../helpers/auth';

const AdminRoute = ({ children }) => (
    isAuth && isAuth().role === 'admin' ? children : <Navigate to="/signin" />
);

export default AdminRoute;